# Integral Design System

### Integral
*adj* necessary, basic


Integral is an abstract representation of popular UX design patterns.

Integral allows you to build a foundation of personality and interaction quickly, before you even consider aesthetic choices.
